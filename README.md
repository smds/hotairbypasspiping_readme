The hot air bypass piping assembly connects the hot air bypass valve to the post combustor plenum. It consists of a single pipe segment. It is implemented as a subassembly for consistency and ease management of its corresponding parameter sets and initial conditions.

This sub-assembly is self-contained and does not require specifying any physical parameters.